(function () {
    'use strict';

    angular
        .module('app')
        .controller('ordemServicoCtrl', ordemServicoCtrl);

    ordemServicoCtrl.$inject = ['$rootScope', '$mdDialog', '$scope', 'sessionService', 'EmpresaService', '$mdExpansionPanel', 'OrdemServicoService', 'toastService', '$state', 'TipoUsuario', 'Status'];

    function ordemServicoCtrl($rootScope, $mdDialog, $scope, sessionService, EmpresaService, $mdExpansionPanel, OrdemServicoService, toastService, $state, TipoUsuario, Status) {

        var os = this;
        var _clean = angular.copy(os.ordemServico);
        var _user = sessionService.getSession();

        os.permissaoEditar = false;
        os.tab = ['Listar Ordens de serviço', 'Gerar Ordem de serviço'];
        os.loading = false;
        os.loadingOrdensServicos = [];
        os.empresaSelecionada = {};
        os.detalhes = {};
        os.empresas = [];
        os.ordensServicos = [];
        os.itens = [];
        os.targetTab = 0;
        os.addRow = _addRow;
        os.removeRow = _removeRow;
        os.limpar = _limpar;
        os.tabCadastro = _tabCadastro;
        os.tabListagem = _tabListagem;
        os.cadastrarOrdemServico = _cadastrarOrdemServico;
        os.getDetalhes = _getDetalhes;
        os.inicioAnalise = _inicioAnalise;
        os.fimAnalise = _fimAnalise;
        os.autorizacao = _autorizacao;
        os.conserto = _conserto;
        os.deslocamento = _deslocamento;
        os.encerrar = _encerrar;
        os.imprimir = _imprimir;
        os.validarGarantia = _validarGarantia;
        os.editar = _editar;
        os.editarOrdemServico = _editarOrdemServico;

        $rootScope.$on('_getOrdensServicos', function () {
            os.ordensServicos = [];
            _getOrdensServicos(_user.id);
        });

        _getEmpresas();
        _getOrdensServicos();

        function _getEmpresas() {
            os.empresas = [];
            EmpresaService.getClientes(_user.empresa.id).then(function (resp) {
                os.empresas = resp;
            }).catch(function () {
                toastService.error('Erro ao recuperar empresas.');
            }).finally(function () {
                ch.loading = false;
            });
        }

        function _getOrdensServicos() {
            os.loadingOrdensServicos = true;
            OrdemServicoService.getAll().then(function (resp) {
                if (resp.length > 0) {
                    os.ordensServicos = _statusColor(resp);
                }
            }).catch(function () {
                toastService.error('Erro ao recuperar ordens de serviços.');
            }).finally(function () {
                os.loadingOrdensServicos = false;
            });
        }

        function _addRow(item) {

            if (item == undefined) return;

            os.itens.push({
                'equipamento': item.equipamento,
                'numeroSerie': item.numeroSerie,
                'modelo': item.modelo,
                'localizacao': item.localizacao
            });
            os.item.equipamento = '';
            os.item.numeroSerie = '';
            os.item.modelo = '';
            os.item.localizacao = '';
        }

        function _removeRow(equipamento) {
            var index = -1;
            var comArr = eval(os.itens);
            for (var i = 0; i < comArr.length; i++) {
                if (comArr[i].equipamento === equipamento) {
                    index = i;
                    break;
                }
            }
            os.itens.splice(index, 1);
        }

        function _tabCadastro() {
            _limpar();
            os.targetTab = 1;
        }

        function _tabListagem() {
            _limpar();
            os.tab[1] = 'Gerar Ordem de serviço';
        }

        function _limpar() {
            os.ordemServico = angular.copy(_clean);
            os.itens = [];
            os.item = {};
            os.buscaEmpresa = {};

            if ($scope.formOrdemServico) {
                $scope.formOrdemServico.$setPristine();
                $scope.formOrdemServico.defeitoInformado.$touched = false;
            }
            $mdExpansionPanel('panel-equipamentos').collapse();
        }

        function _cadastrarOrdemServico(ordemServico) {
            os.loading = true;

            ordemServico.usuario = _user;
            ordemServico.empresa = os.selectedItem;
            ordemServico.equipamentos = os.itens;

            OrdemServicoService.cadastrar(ordemServico).then(function (result) {
                toastService.success('Cadastro efetuada.');
                _getOrdensServicos();
                os.targetTab = 0;
            }).catch(function () {
                toastService.error('Erro ao cadastrar.');
            }).finally(function () {
                os.loading = false;
            });
        }

        function _getDetalhes(ordemServico) {
            os.detalhes = {};
            os.loadingDetalhes = true;

            OrdemServicoService.getOs(ordemServico.id).then(function (resp) {
                os.detalhes = _statusColor(resp);
                _permissaoEditar(os.detalhes);
            }).catch(function () {
                toastService.error('Erro ao recuperar ordem de serviço.');
            }).finally(function () {
                os.loadingDetalhes = false;
            });
        }

        function _statusColor(ordemServico) {
            if (ordemServico.length) {
                for (var i = 0; i < ordemServico.length; i++) {
                    switch (ordemServico[i].status.id) {
                        case 1:
                            ordemServico[i].status.cor = {
                                'color': 'yellowgreen'
                            };
                            break;
                        case 2:
                            ordemServico[i].status.cor = {
                                'color': 'blue'
                            };
                            break;
                        case 3:
                            ordemServico[i].status.cor = {
                                'color': 'orange'
                            };
                            break;
                        case 4:
                            ordemServico[i].status.cor = {
                                'color': 'green'
                            };
                            break;
                        case 5:
                            ordemServico[i].status.cor = {
                                'color': 'red'
                            };
                            break;
                        case 6:
                            ordemServico[i].status.cor = {
                                'color': 'gray'
                            };
                            break;
                        default:
                            break;
                    }
                }
            } else {
                switch (ordemServico.status.id) {
                    case 1:
                        ordemServico.status.cor = {
                            'color': 'yellowgreen'
                        };
                        break;
                    case 2:
                        ordemServico.status.cor = {
                            'color': 'blue'
                        };
                        break;
                    case 3:
                        ordemServico.status.cor = {
                            'color': 'orange'
                        };
                        break;
                    case 4:
                        ordemServico.status.cor = {
                            'color': 'green'
                        };
                        break;
                    case 5:
                        ordemServico.status.cor = {
                            'color': 'red'
                        };
                        break;
                    case 6:
                        ordemServico.status.cor = {
                            'color': 'gray'
                        };
                        break;
                    default:
                        break;
                }
            }
            return ordemServico;
        }

        function _inicioAnalise(ev, ordemServico) {
            $mdDialog.show({
                controller: 'analiseCtrl',
                controllerAs: 'an',
                templateUrl: 'partial/analise.dialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    dataInfo: {
                        ordemServico: ordemServico,
                        tipoAnalise: 1
                    }
                },
                fullscreen: true
            });
        }

        function _fimAnalise(ev, ordemServico) {
            $mdDialog.show({
                controller: 'analiseCtrl',
                controllerAs: 'an',
                templateUrl: 'partial/analise.dialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    dataInfo: {
                        ordemServico: ordemServico,
                        tipoAnalise: 2
                    }
                },
                fullscreen: true
            });
        }

        function _autorizacao(ev, ordemServico) {
            $mdDialog.show({
                controller: 'autorizacaoCtrl',
                controllerAs: 'au',
                templateUrl: 'partial/autorizacao.dialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    dataInfo: {
                        ordemServico: ordemServico
                    }
                },
                fullscreen: true
            });
        }

        function _conserto(ev, ordemServico) {
            $mdDialog.show({
                controller: 'consertoCtrl',
                controllerAs: 'cs',
                templateUrl: 'partial/conserto.dialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    dataInfo: {
                        ordemServico: ordemServico
                    }
                },
                fullscreen: true
            });
        }

        function _deslocamento(ev, ordemServico) {
            $mdDialog.show({
                controller: 'deslocamentoCtrl',
                controllerAs: 'ds',
                templateUrl: 'partial/deslocamento.dialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    dataInfo: {
                        ordemServico: ordemServico
                    }
                },
                fullscreen: true
            });
        }

        function _encerrar(ev, ordemServico) {

            var confirm = $mdDialog.confirm()
                .title('Deseja realmente encerrar esta ordem de serviço?')
                .textContent('Não será possivel interagir com esta ordem de serviço posteriormente.')
                .ariaLabel('Confirmar encerramento da ordem de serviço')
                .targetEvent(ev)
                .ok('Sim')
                .cancel('Não');

            $mdDialog.show(confirm).then(function () {
                os.loadingOrdensServicos = true;
                OrdemServicoService.setFecharOs(ordemServico.id).then(function (resp) {
                    toastService.success('Ordem de serviço encerrada.');
                    _getOrdensServicos();
                }).catch(function () {
                    toastService.error('Erro ao encerrar ordem de serviço.');
                }).finally(function () {
                    os.loadingOrdensServicos = false;
                });
            }, function () {
                console.log('cancelado');
            });
        }

        function _imprimir(ordemServico) {
            $state.go('inicial.impressao', {
                'id': ordemServico.id
            });
        }

        function _validarGarantia(numeroOs) {
            os.detalhes = {};
            os.loadingDetalhes = true;

            OrdemServicoService.validarGarantia(numeroOs).then(function (resp) {
                os.detalhes = _statusColor(resp);
            }).catch(function () {
                toastService.error('Erro ao recuperar ordem de serviço.');
            }).finally(function () {
                os.loadingDetalhes = false;
            });
        }

        function _editar(ordemServico) {
            _tabCadastro();
            os.ordemServico = ordemServico;
            os.selectedItem = ordemServico.empresa;
            os.itens = ordemServico.equipamentos;
        }

        function _editarOrdemServico(ordemServico) {
            os.loading = true;

            OrdemServicoService.editar(ordemServico).then(function (result) {
                toastService.success('Edição efetuada.');
                _getOrdensServicos();
                os.targetTab = 0;
            }).catch(function () {
                toastService.error('Erro ao editar.');
            }).finally(function () {
                os.loading = false;
            });
        }

        function _permissaoEditar(ordemServico){

            if(ordemServico.usuario.id != _user.id){
                 os.permissaoEditar = false;
                 return;
            }

            if(ordemServico.status.id == Status.Aberto || ordemServico.status.id == Status.Atribuido){
                os.permissaoEditar = true;
            }else{
                 os.permissaoEditar = false;
            }
        }
    }
})();