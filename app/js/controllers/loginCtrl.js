(function() {
    'use strict';

    angular.module('app')
        .controller('loginCtrl', loginCtrl);

    loginCtrl.$inject = ['AcessoService', 'Api', 'sessionService', '$state', 'toastService', 'key', 'UsuarioService'];

    function loginCtrl(AcessoService, Api, sessionService, $state, toastService, key, UsuarioService) {

        sessionService.clear();

        var lg = this;
        lg.login = '';
        lg.senha = '';
        lg.loading = false;
        lg.loginBox = true;
        lg.footerinfo = {
            year: new Date().getFullYear()
        };

        lg.login = _login;
        lg.primeiroAcesso = _primeiroAcesso;
        lg.loginPrimeiroAcesso = _loginPrimeiroAcesso;
        lg.voltar = _voltar;

        function _login(loginForm) {
            lg.loading = true;

            var credentials = {
                login: loginForm.login,
                senha: CryptoJS.SHA1(JSON.stringify(loginForm.senha), key).toString()
                    //senha: loginForm.senha
            };

            AcessoService.login(credentials).then(function(resp) {
                sessionService.setSession(resp);
                // $state.go('inicial');
                $state.go('inicial.ordemservico');
            }).catch(function(result) {
                toastService.error('Acesso inválido.')
            }).finally(function() {
                lg.loading = false;
            });
        }

        function _primeiroAcesso() {
            lg.loginBox = false;
        }

        function _voltar() {
            lg.loginBox = true;
        }

        function _loginPrimeiroAcesso(usuario) {

            if (usuario.senha != usuario.repetirSenha) {
                toastService.error('Senhas não são iguais.');
                usuario.senha = '';
                usuario.repetirSenha = '';
                return;
            }

            var credentials = {
                login: usuario.login,
                senha: CryptoJS.SHA1(JSON.stringify(usuario.senha), key).toString()
            };

            AcessoService.primeiroAcesso(credentials).then(function(resp) {
                sessionService.setSession(resp);
                // $state.go('inicial');
                $state.go('inicial.ordemservico');
            }).catch(function(result) {
                toastService.error('Acesso inválido.')
            }).finally(function() {
                lg.loading = false;
            });
        }
    }
})();