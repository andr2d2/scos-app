(function() {
    'use strict';

    angular
        .module('app')
        .controller('analiseCtrl', analiseCtrl);

    analiseCtrl.$inject = ['$mdDialog', 'dataInfo', 'sessionService', 'UsuarioService', 'OrdemServicoService', 'toastService', '$rootScope'];

    function analiseCtrl($mdDialog, dataInfo, sessionService, UsuarioService, OrdemServicoService, toastService, $rootScope) {

        var an = this;
        var _user = sessionService.getSession();
        an.loading = false;
        an.tipoAnalise = 0;
        an.itens = [];
        an.tecnicoAtual = _user;
        an.cancel = _cancel;
        an.inicioAnalise = _inicioAnalise;
        an.addRow = _addRow;
        an.removeRow = _removeRow;
        an.fimAnalise = _fimAnalise;

        _init();
        _getUsuario(_user.empresa.id);

        function _init(tipoAnalise) {
            an.tipoAnalise = dataInfo.tipoAnalise;
        }

        function _getUsuario(empresaId) {
            UsuarioService.getUsuarios(empresaId).then(function(resp) {
                an.tecnicos = resp;
            }).catch(function() {
                toastService.error('Erro ao recuperar usuários.');
            });
        }

        function _inicioAnalise() {
            an.loading = true;
            var obj = {
                id: dataInfo.ordemServico.id,
                inicioAnaliseOs: {
                    tecnicoInicioAnalise: an.tecnicoSelecionado
                }
            };

            OrdemServicoService.setInicioAnalise(obj).then(function(resp) {
                _cancel()
                toastService.success('Análise iniciada.');
                $rootScope.$broadcast('_getOrdensServicos');
            }).catch(function() {
                toastService.error('Erro ao iniciar análise.');
            }).finally(function() {
                an.loading = false;
            });
        }

        function _fimAnalise() {
            an.loading = true;

            var obj = {
                id: dataInfo.ordemServico.id,
                fimAnaliseOs: {
                    descricaoAnalise: an.descricaoAnalise,
                    tecnicoFimAnalise: an.tecnicoSelecionado,
                    pecas: an.itens
                }
            };

            OrdemServicoService.setFimAnalise(obj).then(function(resp) {
                _cancel();
                toastService.success('Análise concluida.');
                $rootScope.$broadcast('_getOrdensServicos');
            }).catch(function() {
                toastService.error('Erro ao concluir análise.');
            }).finally(function() {
                an.loading = false;
            });
        }

        function _addRow(item) {

            if (item == undefined) return;

            an.itens.push({
                'codigo': item.codigo,
                'peca': item.peca,
                'quantidade': item.qtd
            });
            an.item.codigo = '';
            an.item.peca = '';
            an.item.qtd = 0;
        }

        function _removeRow(codigo) {
            var index = -1;
            var comArr = eval(an.itens);
            for (var i = 0; i < comArr.length; i++) {
                if (comArr[i].codigo === codigo) {
                    index = i;
                    break;
                }
            }
            an.itens.splice(index, 1);
        }

        function _cancel() {
            $mdDialog.cancel();
        }
    }
})();