(function() {
    'use strict';

    angular
        .module('app')
        .controller('usuarioCtrl', usuarioCtrl);

    usuarioCtrl.$inject = ['$rootScope', '$mdDialog', '$scope', 'sessionService', 'UsuarioService', '$mdExpansionPanel', 'FuncionarioService', 'toastService', '$state'];

    function usuarioCtrl($rootScope, $mdDialog, $scope, sessionService, UsuarioService, $mdExpansionPanel, FuncionarioService, toastService, $state) {

        var us = this;
        var _clean = angular.copy(us.usuario);
        var _user = sessionService.getSession();
        us.tab = ['Lista de usuários', 'Cadastrar'];
        us.usuarios = [];
        us.tiposUsuarios = [];
        us.detalhes = {};
        us.usuario = {};
        us.loading = false;
        us.getDetalhes = _getDetalhes;
        us.editar = _editar;
        us.limpar = _limpar;
        us.tabCadastro = _tabCadastro;
        us.tabListagem = _tabListagem;
        us.editarUsuario = _editarUsuario;
        us.cadastrarUsuario = _cadastrarUsuario;
        us.resetarSenha = _resetarSenha;



        _getUsuarios();
        _getTipoUsuarios();

        function _tabCadastro() {
            _limpar();
            us.targetTab = 1;
        }

        function _tabListagem() {
            _limpar();
            us.tab[1] = 'Cadastrar';
        }

        function _getUsuarios() {
            var empresaId = _user.empresa.id;

            FuncionarioService.getFuncionarios(empresaId).then(function(resp) {
                us.usuarios = resp;
            }).catch(function() {
                toastService.error('Erro ao recuperar usuarios.');
            }).finally(function() {
                us.loading = false;
            });
        }

        function _getTipoUsuarios() {
            UsuarioService.getTipoUsuarios().then(function(resp) {
                us.tiposUsuarios = resp;
            }).catch(function() {
                toastService.error('Erro ao recuperar usuarios.');
            }).finally(function() {});
        }

        function _getDetalhes(usuario) {
            us.detalhes = {};
            us.loadingDetalhes = true;

            UsuarioService.getUsuario(usuario.id).then(function(resp) {
                us.detalhes = resp;
            }).catch(function() {
                toastService.error('Erro ao recuperar usuário.');
            }).finally(function() {
                us.loadingDetalhes = false;
            });
        }

        function _editar(usuario) {
            us.tab[1] = 'Editar'
            _limpar();
            us.targetTab = 1;
            us.usuario = usuario;
        }

        function _editarUsuario(usuario) {

            usuario.empresa = _user.empresa;

            UsuarioService.editar(usuario).then(function(resp) {
                _getUsuarios();
                us.targetTab = 0;
                toastService.success('Edição efetuada.');
            }).catch(function(result) {
                toastService.erro('Erro ao editar.');
            }).finally(function() {
                us.loading = false;
            });
        }

        function _limpar() {
            us.usuario = {};

            if ($scope.formUsuario) {
                $scope.formUsuario.$setPristine();
            }
        }


        function _cadastrarUsuario(usuario) {

            us.loading = true;
            usuario.empresa = _user.empresa;

            UsuarioService.cadastrar(usuario).then(function(resp) {
                _getUsuarios();
                toastService.success('Cadastro efetuado.');
                us.targetTab = 0;
            }).catch(function(result) {
                toastService.success('Erro ao cadastrar.');
            }).finally(function() {
                us.loading = false;
            });
        }

        function _resetarSenha(ev, usuario) {
            var confirm = $mdDialog.confirm()
                .title('Deseja realmente resetar a senha deste usuário?')
                .textContent('Uma nova senha terá que ser cadastrada no primeiro login.')
                .ariaLabel('Confirmar resete de senha?')
                .targetEvent(ev)
                .ok('Sim')
                .cancel('Não');

            $mdDialog.show(confirm).then(function() {
                us.loadingDetalhes = true;
                UsuarioService.resetSenha(usuario.id).then(function(resp) {
                    toastService.success('Senha resetada.');
                    _getUsuarios();
                }).catch(function() {
                    toastService.error('Erro ao resetar senha.');
                }).finally(function() {
                    us.loadingDetalhes = false;
                });
            }, function() {
                console.log('cancelado');
            });
        }
    }
})();