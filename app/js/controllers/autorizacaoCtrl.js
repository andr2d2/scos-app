(function() {
    'use strict';

    angular
        .module('app')
        .controller('autorizacaoCtrl', autorizacaoCtrl);

    autorizacaoCtrl.$inject = ['$mdDialog', 'OrdemServicoService', 'dataInfo', 'toastService', '$rootScope'];

    function autorizacaoCtrl($mdDialog, OrdemServicoService, dataInfo, toastService, $rootScope) {

        var au = this;
        au.loading = false;
        au.cancel = _cancel;
        au.autorizar = _autorizar;

        function _cancel() {
            $mdDialog.cancel();
        }

        function _autorizar() {
            au.loading = true;
            var obj = {
                id: dataInfo.ordemServico.id,
                autorizacao: {
                    responsavelAutorizacao: au.responsavel,
                    autorizacao: +au.autorizacao
                }
            };

            OrdemServicoService.setAutorizacao(obj).then(function(resp) {
                _cancel();
                toastService.success('Autorização concluida.');
                $rootScope.$broadcast('_getOrdensServicos');
            }).catch(function() {
                toastService.error('Erro ao autorizar.');
            }).finally(function() {
                au.loading = false;
            });
        }
    }
})();