(function() {
    'use strict';

    angular
        .module('app')
        .controller('deslocamentoCtrl', deslocamentoCtrl);

    deslocamentoCtrl.$inject = ['$mdDialog', 'dataInfo', 'sessionService', 'UsuarioService', 'OrdemServicoService', 'toastService', '$rootScope'];

    function deslocamentoCtrl($mdDialog, dataInfo, sessionService, UsuarioService, OrdemServicoService, toastService, $rootScope) {

        var ds = this;
        ds.loading = false;
        ds.cancel = _cancel;
        ds.deslocamento = _deslocamento;
        ds.somaTempo = _somaTempo;

        function _cancel() {
            $mdDialog.cancel();
        }

        function _deslocamento(des) {
            ds.loading = true;
            var obj = {
                id: dataInfo.ordemServico.id,
                deslocamaneto: {
                    KmFinal: ds.kmFinal,
                    KmInicial: ds.kmInicial,
                    KmTotal: ds.kmFinal - ds.kmInicial,
                    deslocamentoSaida: ds.hrSaida,
                    deslocamentoChegada: ds.hrChegada,
                    deslocamentoTotal: ds.hrTotal
                }
            };

            OrdemServicoService.setDeslocamento(obj).then(function(resp) {
                _cancel();
                toastService.success('Deslocamento concluida.');
                $rootScope.$broadcast('_getOrdensServicos');
            }).catch(function() {
                toastService.error('Erro ao concluir deslocamento.');
            }).finally(function() {
                ds.loading = false;
            });
        }

        function _somaTempo(ds) {
            var total = 0;

            total += _toSeconds(ds.hrChegada);
            total -= _toSeconds(ds.hrSaida);
            ds.hrTotal = _toHHMMSS(total);
        }

        function _toSeconds(time) {
            var parts = time.split(':');
            return (+parts[0]) * 60 * 60 + (+parts[1]) * 60 + (+parts[2]);
        }

        function _toHHMMSS(sec) {
            var sec_num = parseInt(sec, 10); // don't forget the second parm
            var hours = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours < 10) {
                hours = "0" + hours;
            }
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            if (seconds < 10) {
                seconds = "0" + seconds;
            }
            var time = hours + ':' + minutes + ':' + seconds;
            return time;
        }
    }
})();