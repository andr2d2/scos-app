(function() {
    'use strict';

    angular.module('app')
        .controller('mainCtrl', mainCtrl);

    mainCtrl.$inject = ['$mdSidenav', 'sessionService', '$state', '$rootScope', 'TipoUsuario'];

    function mainCtrl($mdSidenav, sessionService, $state, $rootScope, TipoUsuario) {

        var mn = this;
        var user = sessionService.getSession();
        mn.nome = user.nomeCompleto;
        mn.tipo = user.tipoUsuario.tipo;
        mn.menu = _menu;
        mn.logout = _logout;

        // mn.menus = [{
        //     state: 'inicial',
        //     nome: 'Home',
        //     icon: 'home'
        // }, {
        //     state: 'inicial.mapa',
        //     nome: 'Mapa',
        //     icon: 'map'
        // }, {
        //     state: 'inicial.empresa',
        //     nome: 'Clientes',
        //     icon: 'location_city'
        // }, {
        //     state: 'inicial.chamado',
        //     nome: 'Chamado',
        //     icon: 'perm_phone_msg'
        // }, {
        //     state: 'inicial.chat',
        //     nome: 'Chat',
        //     icon: 'question_answer'
        // }, {
        //     state: 'inicial.ordemservico',
        //     nome: 'Ordem de serviço',
        //     icon: 'content_paste'
        // }];

        _pemissaoMenu(user);

        function _pemissaoMenu(usuario) {
            var tipoId = user.tipoUsuario.id;

            switch (tipoId) {
                case TipoUsuario.Tecnico:
                    mn.menus = [{
                        state: 'inicial.empresa',
                        nome: 'Clientes',
                        icon: 'location_city'
                    }, {
                        state: 'inicial.ordemservico',
                        nome: 'Ordem de serviço',
                        icon: 'content_paste'
                    }];
                    break;

                case TipoUsuario.Administrador:
                    mn.menus = [{
                        state: 'inicial.empresa',
                        nome: 'Clientes',
                        icon: 'location_city'
                    }, {
                        state: 'inicial.ordemservico',
                        nome: 'Ordem de serviço',
                        icon: 'content_paste'
                    }, {
                        state: 'inicial.usuario',
                        nome: 'Usuários',
                        icon: 'perm_identity'
                    }];
                    break;

                case TipoUsuario.Dev:
                    mn.menus = [{
                        state: 'inicial',
                        nome: 'Home',
                        icon: 'home'
                    }, {
                        state: 'inicial.mapa',
                        nome: 'Mapa',
                        icon: 'map'
                    }, {
                        state: 'inicial.empresa',
                        nome: 'Clientes',
                        icon: 'location_city'
                    }, {
                        state: 'inicial.chamado',
                        nome: 'Chamado',
                        icon: 'perm_phone_msg'
                    }, {
                        state: 'inicial.chat',
                        nome: 'Chat',
                        icon: 'question_answer'
                    }, {
                        state: 'inicial.ordemservico',
                        nome: 'Ordem de serviço',
                        icon: 'content_paste'
                    }, {
                        state: 'inicial.usuario',
                        nome: 'Usuários',
                        icon: 'perm_identity'
                    }];
                    break;

                default:
                    break;
            }
        }

        function _menu() {
            $mdSidenav('menu').toggle();
        }

        function _logout() {
            $rootScope.$broadcast('_sair');
            sessionService.clear();
            $state.go('login');
        }

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {

            var loggedIdUser = sessionService.getSession().id;
            if (loggedIdUser === undefined && toState.url != '/') {
                _logout();
            }

            $mdSidenav('menu').close();

            if (toState.url != '/chat') {
                $rootScope.$broadcast('sair');
            }
        });
    }
})();