(function() {
    'use strict';;

    angular
        .module('app')
        .service('noteService', noteService);

    angular
        .module('app')
        .controller('chatCtrl', chatCtrl);

    noteService.$inject = ['Hub', '$rootScope', 'sessionService'];

    chatCtrl.$inject = ['$rootScope', 'noteService', 'sessionService'];

    function noteService(Hub, $rootScope, sessionService) {

        var nt = this;
        var userId = sessionService.getSession().id;
        nt.chatObj = [];
        nt.addChat = _addChat;
        nt.send = _send;
        nt.entrar = _entrar;
        nt.sair = _sair;
        nt.connect = _connect;

        var hub = new Hub('ChatHub', {
            listeners: {
                'msgAdded': function(remetente, msg) {
                    _addChat(remetente, msg);
                    $rootScope.$apply();
                },
                'userJoin': function(remetente, msg) {
                    _addChat(remetente, msg);
                    $rootScope.$apply();
                },
                'userLeave': function(remetente, msg) {
                    _addChat(remetente, msg);
                    $rootScope.$apply();
                }
            },
            methods: ['OnConnected', 'OnDisconnected', 'SendMsg', 'Join', 'Leave'],
            //jsonp: true,
            queryParams: {
                'userId': userId
            },
            logging: true,
            autoConnect: true,
            errorHandler: function(error) {
                console.error('Erro: ', error);
            },
            stateChanged: function(state) {
                switch (state.newState) {
                    case $.signalR.connectionState.connecting:
                        console.log('Conectando...');
                        break;
                    case $.signalR.connectionState.connected:
                        console.log('Conectado');
                        break;
                    case $.signalR.connectionState.reconnecting:
                        console.log('Reconectando...');
                        break;
                    case $.signalR.connectionState.disconnected:
                        console.log('Disconectado');
                        $rootScope.$broadcast('sair');
                        break;
                }
            },
            rootPath: 'http://localhost:65165/signalr'
        });

        function _connect(user) {
            hub.Join(user);
        }

        // hub.promise.done(function() {
        //     $rootScope.$broadcast('entrar');
        // });

        // client
        function _addChat(remetente, msg) {
            console.log('Mensagem adicionada...');
            nt.chatObj.push({
                remetente: remetente,
                msg: msg
            });
        }

        //server
        function _send(remetente, msg) {
            console.log('Mensagem enviada...');
            hub.SendMsg(remetente, msg);
        }

        function _entrar(user) {
            console.log('Entrando...');
            hub.Join(user);
        }

        function _sair(user) {
            console.log('Saindo...');
            //hub.Leave(user);
            hub.OnDisconnected();
            nt.chatObj = [];
        }

    }

    function chatCtrl($rootScope, noteService, sessionService) {

        var chat = this;
        var user = sessionService.getSession();
        chat.mensagens = noteService.chatObj;
        chat.sendMsg = _sendMsg;
        chat.sair = _sair;

        noteService.connect(user);

        // $rootScope.$on('entrar', function(event, args) {
        //     noteService.entrar(user);
        //     //chat.mensagens = [];
        // });

        $rootScope.$on('_sair', function(event, args) {
            noteService.sair(user);
        });

        function _sair() {
            noteService.sair();
        }

        function _sendMsg(chat) {
            var remetente = user.nomeCompleto;
            noteService.send(remetente, chat.msg);
        }
    }
})();