(function() {
    'use strict';

    angular
        .module('app')
        .controller('impressaoCtrl', impressaoCtrl);

    impressaoCtrl.$inject = ['$state', '$stateParams', 'OrdemServicoService', 'toastService'];

    function impressaoCtrl($state, $stateParams, OrdemServicoService, toastService) {
        var im = this;

        im.os = {};
        im.cancel = _cancel;

        _getOs();

        function _getOs() {
            var ordemServicoId = $stateParams.id;

            OrdemServicoService.getOs(ordemServicoId).then(function(resp) {
                im.os = resp;
            }).catch(function(result) {
                toastService.error('Erro ao recuperar ordem de serviço.');
            }).finally(function() {
                ch.loading = false;
            });
        }

        function _cancel() {
            $state.go('inicial.ordemservico');
        }
    }
})();