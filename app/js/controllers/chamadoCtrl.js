(function() {
    'use strict';

    angular
        .module('app')
        .controller('chamadoCtrl', chamadoCtrl);

    chamadoCtrl.$inject = ['$state', '$rootScope', '$scope', 'sessionService', 'chamadoService', 'toastService', '$mdExpansionPanel', 'Status', '$linq'];

    function chamadoCtrl($state, $rootScope, $scope, sessionService, chamadoService, toastService, $mdExpansionPanel, Status, $linq) {

        var ch = this;
        var _userId = sessionService.getSession().id;
        var _clean = angular.copy(ch.chamado);

        ch.tab = ['Lista de chamados', 'Cadastrar'];
        ch.targetTab = 0;
        ch.chamadosUsuario = [];
        ch.itens = [];
        ch.chamado = {};
        ch.showItem = false;
        ch.loading = false;
        ch.loadingChamados = false;

        ch.addRow = _addRow;
        ch.removeRow = _removeRow;
        ch.cadastrarChamado = _cadastrarChamado;
        ch.limpar = _limpar;
        ch.editar = _editar;
        ch.editarChamado = _editarChamado;
        ch.tabCadastro = _tabCadastro;
        ch.tabListagem = _tabListagem;

        init();

        $rootScope.$on('_adicionarChamados', function(event, args) {
            _adicionarChamados(args);
        });

        $rootScope.$on('_editarChamadoUsuario', function(event, args) {
            _editarChamadoUsuario(args);
        });

        function init() {
            ch.loadingChamados = true;

            chamadoService.initChamado(_userId).then(function(result) {

            }).catch(function() {
                toastService.error('Problemas ao buscar chamados.');
            }).finally(function() {
                ch.loadingChamados = false;
            });
        }

        function _addRow(item) {

            if (item == undefined) return;

            ch.itens.push({
                'equipamento': item.equipamento,
                'numeroSerie': item.numeroSerie,
                'modelo': item.modelo
            });
            ch.item.equipamento = '';
            ch.item.numeroSerie = '';
            ch.item.modelo = '';
        }

        function _removeRow(equipamento) {
            var index = -1;
            var comArr = eval(ch.itens);
            for (var i = 0; i < comArr.length; i++) {
                if (comArr[i].equipamento === equipamento) {
                    index = i;
                    break;
                }
            }
            ch.itens.splice(index, 1);
        }

        function _cadastrarChamado(chamado) {

            ch.loading = true;
            chamado.usuario = {
                id: _userId
            };

            if (ch.itens.length > 0) chamado.itens = ch.itens;

            chamadoService.cadastrarChamado(chamado).then(function(result) {
                toastService.success('Cadastro efetuada.');
                ch.targetTab = 0;
            }).catch(function(result) {
                toastService.error('Erro ao cadastrar.');
            }).finally(function() {
                ch.loading = false;
            });
        }

        function _editarChamado(chamado) {

            ch.loading = true;

            chamadoService.editarChamado(chamado).then(function(result) {
                toastService.success('Edição efetuada.');
                ch.targetTab = 0;
            }).catch(function(result) {
                toastService.error('Erro ao editar.');
            }).finally(function() {
                ch.loading = false;
            });
        }

        function _limpar() {
            ch.chamado = angular.copy(_clean);
            ch.itens = [];
            ch.item = '';

            if ($scope.formChamado) {
                $scope.formChamado.$setPristine();
                $scope.formChamado.descricao.$touched = false;
            }
            $mdExpansionPanel('panel-equipamentos').collapse();
        }

        function _tabCadastro() {
            _limpar();
            ch.targetTab = 1;
        }

        function _tabListagem() {
            ch.tab[1] = 'Cadastrar';
        }

        function _adicionarChamados(chamado) {
            if (chamado.length > 0) {
                for (var i = 0, len = chamado.length; i < len; i++) {
                    _adicionarChamado(chamado[i]);
                }
            } else {
                _adicionarChamado(chamado);
            }
        }

        function _adicionarChamado(chamado) {
            chamado = _permissaoEditar(chamado);
            ch.chamadosUsuario.push(chamado);
        }

        function _editar(chamado) {
            ch.tab[1] = "Editar"
            ch.targetTab = 1;
            ch.chamado = chamado;
            ch.itens = chamado.itens;
        }

        function _editarChamadoUsuario(chamado) {
            for (var i = 0, len = ch.chamadosUsuario.length; i < len; i++) {
                if (ch.chamadosUsuario[i].id == chamado.id) {
                    chamado = _permissaoEditar(chamado);
                    ch.chamadosUsuario[i] = chamado;
                }
            }
        }

        function _permissaoEditar(chamado) {
            if (chamado.status.id != Status.Aberto)
                chamado.permitidoEditar = false;
            else
                chamado.permitidoEditar = true;

            return chamado;
        }
    }

})();