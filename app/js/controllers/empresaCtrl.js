(function() {
    'use strict';

    angular.module('app')
        .controller('empresaCtrl', empresaCtrl);

    empresaCtrl.$inject = ['$scope', 'EmpresaService', 'GeoService', 'toastService', 'NgMap', 'sessionService', 'TipoUsuario'];

    function empresaCtrl($scope, EmpresaService, GeoService, toastService, NgMap, sessionService, TipoUsuario) {

        var ep = this;
        var user = sessionService.getSession();
        var _clean = angular.copy(ep.empresa);
        ep.isAdm = false;
        ep.tab = ['Lista de clientes', 'Cadastrar'];
        ep.targetTab = 0;
        ep.showInativos = false;
        ep.positions = [];
        ep.empresas = [];
        ep.empresasInativas = [];
        ep.detalhes = {};
        ep.estados = [];
        ep.cidades = [];
        ep.coord = [];

        ep.loading = false;
        ep.detalhesLoading = false;
        ep.mapLoading = false;

        ep.getDetalhes = _getDetalhes;
        ep.getCidades = _getCidades;
        ep.buscaEndereco = _buscaEndereco;
        ep.tabCadastro = _tabCadastro;
        ep.tabListagem = _tabListagem;
        ep.cadastrar = _cadastrar;
        ep.editar = _editar;
        ep.limpar = _limpar;
        ep.salvar = _salvar;
        ep.listarInativos = _listarInativos

        _resetForm();
        _getClientes();
        _getEstados();
        _permissao();

        function _permissao() {
            var tipo = user.tipoUsuario.id;
            if (tipo == TipoUsuario.Administrador) {
                ep.isAdm = user.tipoUsuario.id;
            }
        }

        function _resetForm() {

            ep.empresa = angular.copy(_clean);
            ep.empresasInativas = angular.copy(_clean);
            ep.coord = {};
            ep.empresa = {};

            if ($scope.formEmpresa) {
                $scope.formEmpresa.$setPristine();
            }
        }

        function _getClientes() {
            ep.loading = true;
            ep.showInativos = false;
            ep.empresasInativas = [];
            ep.empresas = [];
            var usuario = sessionService.getSession();

            EmpresaService.getClientes(usuario.empresa.id).then(function(resp) {
                ep.empresas = resp;

            }).catch(function(result) {
                toastService.error('Erro ao listar.');
            }).finally(function() {
                ep.loading = false;
            });
        }

        function _getEstados() {
            GeoService.getEstados().then(function(resp) {
                ep.estados = resp;
            }).catch(function(result) {
                toastService.error('Erro ao recuperar estados.');
            }).finally(function() {});
        }

        function _getCidades(estadoId) {
            ep.cidades = [];

            if (estadoId > 0) {
                GeoService.getCidades(estadoId).then(function(resp) {
                    ep.cidades = resp;
                }).catch(function(result) {
                    toastService.error('Erro ao recuperar cidades.');
                }).finally(function() {});
            }
        }

        function _getDetalhes(empresaId) {

            ep.detalhes = {};
            ep.detalhesLoading = true;
            var _marker = {};

            EmpresaService.getEmpresa(empresaId).then(function(resp) {
                _marker = {
                    lat: resp.coordenadas.latitude,
                    lng: resp.coordenadas.longitude
                };

                ep.detalhes = resp;
                ep.detalhes.coordenadas = _marker;

            }).catch(function(result) {
                toastService.error('Erro ao recuperar detalhes da empresa.');
            }).finally(function() {
                ep.detalhesLoading = false;
            });
        }

        function _buscaEndereco(empresa) {

            ep.coord = {};
            ep.mapLoading = true;
            var address = empresa.endereco + ', ' + empresa.numero + ', ' + empresa.bairro + ', ' + empresa.cep;

            NgMap.getGeoLocation(address).then(function(result) {

                NgMap.getMap({
                    id: 'mapCadastro'
                }).then(function(map) {
                    var map = map;
                    ep.coord = result;
                    map.setCenter(result);
                }).catch(function(result) {
                    //toastService.success('Erro ao cadastrar.');
                }).finally(function() {
                    ep.mapLoading = false;
                });
            });
        }

        function _tabListagem() {
            ep.tab[1] = 'Cadastrar';
            _resetForm();
            $scope.formEmpresa.$setPristine();
        }

        function _tabCadastro() {
            _resetForm();
            ep.targetTab = 1;
        }

        function _cadastrar(empresa) {

            ep.loading = true;
            empresa.coordenadas = {
                latitude: parseFloat(ep.coord.lat()),
                longitude: parseFloat(ep.coord.lng())
            };
            empresa.usuarioInteracao = sessionService.getSession();

            EmpresaService.cadastrar(empresa).then(function(resp) {
                _getClientes();
                toastService.success('Cadastro efetuado.');
                ep.targetTab = 0;
            }).catch(function(result) {
                toastService.success('Erro ao cadastrar.');
            }).finally(function() {
                ep.loading = false;
            });

        }

        function _editar(empresa) {

            ep.tab[1] = 'Editar'
            _resetForm();
            ep.targetTab = 1;
            ep.empresa = empresa;

            var marker = {
                lat: ep.detalhes.coordenadas.lat,
                lng: ep.detalhes.coordenadas.lng
            };

            NgMap.getMap({
                id: 'mapCadastro'
            }).then(function(map) {
                var map = map;
                map.setCenter(marker);
                ep.coord = marker;
            });

            setTimeout(function() {
                _getCidades(empresa.estado.id);
            }, 500);
        }

        function _limpar() {
            _resetForm();
        }

        function _salvar(empresa) {

            NgMap.getMap({
                id: 'mapCadastro'
            }).then(function(map) {
                var map = map;
                var marker = map.markers[0].getPosition();

                empresa.coordenadas = {
                    latitude: marker.lat(),
                    longitude: marker.lng()
                };

                EmpresaService.editar(empresa).then(function(resp) {
                    _getClientes();
                    ep.targetTab = 0;
                    toastService.success('Edição efetuada.');
                }).catch(function(result) {
                    toastService.erro('Erro ao editar.');
                }).finally(function() {
                    ep.loading = false;
                });
            });
        }

        function _listarInativos() {
            ep.loading = true;
            var usuario = sessionService.getSession();

            if (!ep.showInativos) {
                EmpresaService.getClientesInativos(usuario.empresa.id).then(function(resp) {
                    if (resp.length > 0) {
                        ep.empresasInativas = resp;
                        ep.showInativos = true;
                    } else {
                        toastService.alert('Não há clientes inativos.');
                    }
                }).catch(function(result) {
                    toastService.error('Erro ao listar.');
                }).finally(function() {
                    ep.loading = false;
                });
            } else {
                ep.showInativos = false;
                ep.empresasInativas = [];
                ep.loading = false;
            }
        }
    }
})();