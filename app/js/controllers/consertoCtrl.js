(function() {
    'use strict';

    angular
        .module('app')
        .controller('consertoCtrl', consertoCtrl);

    consertoCtrl.$inject = ['$mdDialog', 'dataInfo', 'sessionService', 'UsuarioService', 'OrdemServicoService', 'toastService', '$rootScope'];

    function consertoCtrl($mdDialog, dataInfo, sessionService, UsuarioService, OrdemServicoService, toastService, $rootScope) {

        var cs = this;
        var _user = sessionService.getSession();
        cs.loading = false;
        cs.tipoAnalise = 0;
        cs.itens = [];
        cs.tecnicoAtual = _user;
        cs.cancel = _cancel;
        cs.conserto = _conserto;

        _getUsuario(_user.empresa.id);

        function _getUsuario(empresaId) {
            UsuarioService.getUsuarios(empresaId).then(function(resp) {
                cs.tecnicos = resp;
            }).catch(function() {
                toastService.error('Erro ao recuperar usuário.');
            });
        }

        function _cancel() {
            $mdDialog.cancel();
        }

        function _conserto() {
            cs.loading = true;

            var obj = {
                id: dataInfo.ordemServico.id,
                conserto: {
                    tecnicoConserto: cs.tecnicoSelecionado,
                    descricaoConserto: cs.descricao
                }
            };

            OrdemServicoService.setConserto(obj).then(function(resp) {
                _cancel()
                toastService.success('Conserto concluído.');
                $rootScope.$broadcast('_getOrdensServicos');
            }).catch(function() {
                toastService.error('Erro ao concluir conserto.');
            }).finally(function() {
                cs.loading = false;
            });
        }
    }
})();