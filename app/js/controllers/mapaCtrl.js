(function() {
    'use strict';

    angular
        .module('app')
        .controller('mapaCtrl', mapaCtrl);

    mapaCtrl.$inject = ['$scope', '$rootScope', 'chamadoService', 'NgMap', 'toastService', '$linq', '$templateRequest', '$sce', '$compile'];

    function mapaCtrl($scope, $rootScope, chamadoService, NgMap, toastService, $linq, $templateRequest, $sce, $compile) {

        var mp = this;
        var _map = {};
        var win = new google.maps.InfoWindow();

        mp.chamado = {};
        mp.loading = false;
        mp.loadingAtender = false;

        mp.infoWindow = _infoWindow;
        mp.atender = _atender;


        init();

        $rootScope.$on('_adicionarPinChamado', function(event, args) {
            _adicionarPinChamado(args);
        });


        function init() {
            mp.loading = true;

            chamadoService.initMap().then(function(pins) {
                _initMap(pins);
            }).catch(function() {
                toastService.error('Problemas ao popular mapa.');
            }).finally(function() {
                mp.loading = false;
            });
        }

        function _initMap(pins) {
            NgMap.getMap({
                id: 'map-chamado'
            }).then(function(map) {
                _map = map;

                for (var i = 0, len = pins.length; i < len; i++) {
                    _adicionarPinChamado(pins[i]);
                }
            });
        }

        function _infoWindow(e, chamado) {
            mp.chamado = chamado;
            _map.showInfoWindow('infoWindow', this);
        }

        function _atender() {
            chamadoService.atender(mp.chamado).then(function(result) {
                mp.loadingAtender = true;
            }).catch(function() {
                toastService.error('Problemas ao efetuar atendimento.');
            }).finally(function() {
                mp.loadingAtender = false;
            });
        }

        function _adicionarPinChamado(pin) {

            var lat = pin.empresa.coordenadas.latitude;
            var lng = pin.empresa.coordenadas.longitude;
            var coord = new google.maps.LatLng(lat, lng);

            var marker = new google.maps.Marker({
                position: coord,
                title: pin.empresa.nomeFantasia,
                customInfo: pin,
                animation: google.maps.Animation.DROP,
                map: _map
            });

            marker.addListener('click', function() {
                _infoWindow(_map, marker);
            });
        }

        function _infoWindow(map, marker) {

            mp.chamado = marker.customInfo;

            var conteudo = ' <div id="infoWindow"><div>' +
                '<div layout="column" layout-padding layout-align="center-center" ng-if="mp.loadingAtender == true">' +
                '<md-progress-circular md-mode="indeterminate"></md-progress-circular>' +
                '<span>Preparando atendimento...</span>' +
                '</div>' +
                '<md-list flex>' +
                '<div ng-if="mp.loadingAtender == false">' +
                '<md-list-item class="md-3-line">' +
                '<div class="md-list-item-text" layout="column">' +
                '<h3>{{mp.chamado.empresa.nomeFantasia}}</h3>' +
                '<h4>{{mp.chamado.empresa.endereco}}, {{mp.chamado.empresa.numero}}</h4>' +
                '<p>{{mp.chamado.status.status}}</p>' +
                '</div>' +
                '</md-list-item>' +
                '<md-button layout-fill class="md-raised md-warn" aria-label="Atender" ng-click="mp.atender()">Atender chamado</md-button>' +
                '</div>' +
                '</md-list>' +
                '</div></div>';
            var compiled = $compile(conteudo)($scope);
            $scope.$apply();

            win.setContent(compiled[0].innerHTML);
            win.open(map, marker);
        }
    }
})();