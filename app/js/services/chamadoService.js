(function() {
    'use strict';

    angular
        .module('app')
        .service('chamadoService', chamadoService);

    chamadoService.$inject = ['$q', 'Hub', '$rootScope', 'toastService', 'chamadoDataService'];

    function chamadoService($q, Hub, $rootScope, toastService, chamadoDataService) {

        var c = this;
        c.cadastrarChamado = _cadastrarChamado;
        c.editarChamado = _editarChamado;
        c.atender = _atender;
        c.initChamado = _initChamado;
        c.initMap = _initMap;

        var hub = new Hub('ChamadoHub', {
            listeners: {
                'novoChamadoUsuario': function(chamado) {
                    _novoChamadoUsuario(chamado);
                    $rootScope.$apply();
                },
                'novoChamadoMap': function(pin) {
                    _novoChamadoMap(pin);
                    $rootScope.$apply();
                },
                'editarChamadoUsuario': function(chamadoEditado) {
                    _editarChamadoUsuario(chamadoEditado);
                    $rootScope.$apply();
                }
            },
            methods: ['CadastrarChamado', 'GetChamados', 'GetChamadosMap', 'Atender', 'EditarChamado'],
            logging: true,
            autoConnect: true,
            errorHandler: function(error) {
                toastService.error('Erro de conexão: ' + error.message);
            },
            stateChanged: function(state) {
                switch (state.newState) {
                    case $.signalR.connectionState.connecting:
                        console.log('Conectando...');
                        break;
                    case $.signalR.connectionState.connected:
                        console.log('Conectado');
                        break;
                    case $.signalR.connectionState.reconnecting:
                        console.log('Reconectando...');
                        break;
                    case $.signalR.connectionState.disconnected:
                        console.log('Disconectado');
                        $rootScope.$broadcast('sair');
                        break;
                }
            },
            rootPath: 'http://localhost:65165/signalr'
        });



        // Client
        function _novoChamadoUsuario(chamado) {
            $rootScope.$broadcast('_adicionarChamados', chamado);
        }

        function _novoChamadoMap(pin) {
            $rootScope.$broadcast('_adicionarPinChamado', pin);
        }

        function _editarChamadoUsuario(chamadoEditado) {
            $rootScope.$broadcast('_editarChamadoUsuario', chamadoEditado);
        }

        // Server
        function _initChamado(userId) {
            var deferred = $q.defer();

            hub.promise.done(function() {
                hub.GetChamados(userId).done(function(chamados) {
                    if (chamados) {
                        if (chamados.length > 0)
                            $rootScope.$broadcast('_adicionarChamados', chamados);

                        deferred.resolve(1);
                    } else {
                        deferred.reject();
                    }
                });
            });
            return deferred.promise;
        }

        function _initMap() {
            var deferred = $q.defer();

            hub.promise.done(function() {
                hub.GetChamadosMap().done(function(pins) {
                    if (pins)
                        deferred.resolve(pins);
                    else
                        deferred.reject();
                });
            });
            return deferred.promise;
        }

        function _cadastrarChamado(chamado) {
            var deferred = $q.defer();
            hub.CadastrarChamado(chamado).done(function(result) {
                if (result === 3) //TaskStatus.Running;
                    deferred.resolve(result);
                else
                    deferred.reject(result);
            });
            return deferred.promise;
        }

        function _atender(chamado) {
            var deffered = $q.defer();
            hub.Atender(chamado).done(function(result) {
                if (result === 1)
                    deferred.resolve(result);
                else
                    deferred.reject(result);
            });
            return deferred.promise;
        }

        function _editarChamado(chamado) {
            var deferred = $q.defer();
            hub.EditarChamado(chamado).done(function(result) {
                if (result === 3) //TaskStatus.Running;
                    deferred.resolve(result);
                else
                    deferred.reject(result);
            });
            return deferred.promise;
        }
    }
})();