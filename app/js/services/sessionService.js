(function() {
    'use strict';

    angular
        .module('app')
        .service('sessionService', sessionService);

    sessionService.$inject = ['$sessionStorage'];

    function sessionService($sessionStorage) {

        var s = this;
        s.setSession = _setSession;
        s.getSession = _getSession;
        s.clear = _clear;

        function _setSession(obj) {
            $sessionStorage.$default(obj);
        }

        function _getSession() {
            return $sessionStorage.$default();
        }

        function _clear() {
            $sessionStorage.$reset();
        }
    }
})();