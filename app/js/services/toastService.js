(function() {
    'use strict';

    angular
        .module('app')
        .service('toastService', toastService);

    toastService.$inject = ['$mdToast'];

    function toastService($mdToast) {

        var t = this;
        t.error = _error;
        t.success = _success;
        t.alert = _alert;

        function _toast(msg, type) {
            $mdToast.show({
                template: '<md-toast class="md-toast ' + type + '">' + msg + '</md-toast>',
                hideDelay: 3000,
                position: 'bottom left'
            });
        }

        function _error(msg) {
            _toast(msg, 'error');
        }

        function _success(msg) {
            _toast(msg, 'success');
        }

        function _alert(msg) {
            _toast(msg, 'alert');
        }
    }
})();