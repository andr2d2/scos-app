(function () {
    'use strict';

    angular
        .module('scos', [
            'angular.viacep'
        ])
        .constant('key', '354684')
        .constant('StatusCode', {
            OK: 200,
            Created: 201,
            Accepted: 202,
            NoContent: 204,
            NotFound: 404,
            BadRequest: 400
        })
        .constant('Status', {
            Aberto: 1,
            Atribuido: 2,
            Pendente_autorizacao: 3,
            Autorizado: 4,
            Nao_autorizado: 5,
            Encerrado: 6,
            Reparado: 7
        })
        .constant('TipoUsuario', {
            Dev: 1,
            Administrador: 2,
            Cliente: 3,
            Tecnico: 4
        })
        .constant('Api', {
            host: 'http://localhost:65165/api/',
            //host: 'http://breves.no-ip.org:6089/scos_api/api/',
            login: 'Usuario/Login',
            getUsuarios: 'Usuario/Empresa/{empresaId}',
            getAllEmpresa: 'Empresa',
            getClientes: 'Empresa/{empresaId}/Clientes',
            getClientesInativos: 'Empresa/{empresaId}/ClientesInativos',
            getEmpresa: 'Empresa/{empresaId}',
            setEmpresa: 'Empresa',
            putEmpresa: 'Empresa/Cliente/Editar',
            getAll: 'OrdemServico',
            setOrdemServico: 'OrdemServico/Cadastrar',
            getOsUsuario: 'OrdemServico/Usuario/{usuarioId}',
            setAutorizacao: 'OrdemServico/Autorizacao',
            setConserto: 'OrdemServico/Conserto',
            setFecharOs: 'OrdemServico/Fechar',
            setDeslocamento: 'OrdemServico/Deslocamento',
            getOs: 'OrdemServico/{osId}',
            putOrdemServico: 'OrdemServico/Editar',
            setInicioAnalise: 'OrdemServico/InicioAnalise',
            setFimAnalise: 'OrdemServico/FimAnalise',
            getEstados: 'Geo/Estados',
            getEstado: 'Geo/Estados/{estadoId}',
            getCidades: 'Geo/Estado/{estadoId}/Cidades',
            getCidade: 'Geo/Cidade/{cidadeId}',
            getItens: 'Item/',
            setItem: 'Item/',
            getFuncionarios: 'Usuario/Empresa/{empresaId}',
            getTipoUsuarios: 'Usuario/TipoUsuario',
            setChamado: 'Usuario/TipoUsuario',
            getUsuario: 'Usuario/{usuarioId}',
            putUsuario: 'Usuario/Editar',
            setUsuario: 'Usuario/',
            resetSenha: 'Usuario/ResetSenha',
            primeiroAcesso: 'Usuario/PrimeiroAcesso',
        })
        .service('HttpService', HttpService)
        .service('AcessoService', AcessoService)
        .service('EmpresaService', EmpresaService)
        .service('OrdemServicoService', OrdemServicoService)
        .service('GeoService', GeoService)
        .service('ItemService', ItemService)
        .service('FuncionarioService', FuncionarioService)
        .service('UsuarioService', UsuarioService)
        .service('ChamadoService', ChamadoService);


    HttpService.$inject = ['$http'];
    AcessoService.$inject = ['HttpService', 'Api', 'StatusCode', '$q'];
    EmpresaService.$inject = ['HttpService', 'Api', 'StatusCode', '$q'];
    OrdemServicoService.$inject = ['HttpService', 'Api', 'StatusCode', '$q'];
    GeoService.$inject = ['HttpService', 'Api', 'StatusCode', '$q'];
    ItemService.$inject = ['HttpService', 'Api', 'StatusCode', '$q'];
    FuncionarioService.$inject = ['HttpService', 'Api', 'StatusCode', '$q'];
    UsuarioService.$inject = ['HttpService', 'Api', 'StatusCode', '$q'];
    ChamadoService.$inject = ['HttpService', 'Api', 'StatusCode', '$q'];

    function HttpService($http) {
        var s = this;

        s.POST = function (params, url) {
            return $http({
                method: 'POST',
                url: url,
                data: JSON.stringify(params),
            }).then(successCallback, errorCallback);
        }

        s.GET = function (url) {
            return $http({
                method: 'GET',
                url: url
            }).then(successCallback, errorCallback);
        }

        s.PUT = function (params, url) {
            return $http({
                method: 'PUT',
                url: url,
                data: JSON.stringify(params),
            }).then(successCallback, errorCallback);
        }

        function successCallback(response) {
            return response;
        }

        function errorCallback(response) {
            return response;
        }
    }

    function AcessoService(HttpService, Api, StatusCode, $q) {
        var acc = this;

        acc.login = function (params) {
            var deferred = $q.defer();
            return HttpService.POST(params, Api.host + Api.login).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

        acc.primeiroAcesso = function (params) {
            var deferred = $q.defer();
            return HttpService.POST(params, Api.host + Api.primeiroAcesso).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }
    }

    function EmpresaService(HttpService, Api, StatusCode, $q) {
        var emp = this;

        emp.getAll = function () {
            var deferred = $q.defer();
            return HttpService.GET(Api.host + Api.getAllEmpresa).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

        emp.getClientes = function (empresaId) {
            var deferred = $q.defer();
            return HttpService.GET(Api.host + Api.getClientes.replace('{empresaId}', empresaId)).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

        emp.getClientesInativos = function (empresaId) {
            var deferred = $q.defer();
            return HttpService.GET(Api.host + Api.getClientesInativos.replace('{empresaId}', empresaId)).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

        emp.cadastrar = function (params) {
            var deferred = $q.defer();
            return HttpService.POST(params, Api.host + Api.setEmpresa).then(function (resp) {
                if (resp.status === StatusCode.Created) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

        emp.editar = function (params) {
            var deferred = $q.defer();
            return HttpService.PUT(params, Api.host + Api.putEmpresa).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

        emp.getEmpresa = function (empresaId) {
            var deferred = $q.defer();
            return HttpService.GET(Api.host + Api.getEmpresa.replace('{empresaId}', empresaId)).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        };
    }

    function OrdemServicoService(HttpService, Api, StatusCode, $q) {
        var os = this;

        os.cadastrar = function (params) {
            var deferred = $q.defer();
            return HttpService.POST(params, Api.host + Api.setOrdemServico).then(function (resp) {
                if (resp.status === StatusCode.Created) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        };

        os.editar = function (params) {
            var deferred = $q.defer();
            return HttpService.PUT(params, Api.host + Api.putOrdemServico).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        };

        os.getAll = function () {
            var deferred = $q.defer();
            return HttpService.GET(Api.host + Api.getAll).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        };

        os.getOsUsuario = function (usuarioId) {
            var deferred = $q.defer();
            return HttpService.GET(Api.host + Api.getOsUsuario.replace('{usuarioId}', usuarioId)).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        };

        os.getOs = function (osId) {
            var deferred = $q.defer();
            return HttpService.GET(Api.host + Api.getOs.replace('{osId}', osId)).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        };

        os.setInicioAnalise = function (params) {
            var deferred = $q.defer();
            return HttpService.PUT(params, Api.host + Api.setInicioAnalise).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        };

        os.setFimAnalise = function (params) {
            var deferred = $q.defer();
            return HttpService.PUT(params, Api.host + Api.setFimAnalise).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        };

        os.setAutorizacao = function (params) {
            var deferred = $q.defer();
            return HttpService.PUT(params, Api.host + Api.setAutorizacao).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

        os.setConserto = function (params) {
            var deferred = $q.defer();
            return HttpService.PUT(params, Api.host + Api.setConserto).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

        os.setFecharOs = function (params) {
            var deferred = $q.defer();
            return HttpService.POST(params, Api.host + Api.setFecharOs).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

        os.setDeslocamento = function (params) {
            var deferred = $q.defer();
            return HttpService.PUT(params, Api.host + Api.setDeslocamento).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

    }

    function GeoService(HttpService, Api, StatusCode, $q) {
        var geo = this;

        geo.getEstados = function () {
            var deferred = $q.defer();
            return HttpService.GET(Api.host + Api.getEstados).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

        geo.getEstado = function (estadoId) {
            var deferred = $q.defer();
            return HttpService.GET(Api.host + Api.getEstado.replace('{estadoId}', estadoId)).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

        geo.getCidades = function (estadoId) {
            var deferred = $q.defer();
            return HttpService.GET(Api.host + Api.getCidades.replace('{estadoId}', estadoId)).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

        geo.getCidade = function (cidadeId) {
            var deferred = $q.defer();
            return HttpService.GET(Api.host + Api.getCidade.replace('{cidadeId}', cidadeId)).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }
    }

    function ItemService(HttpService, Api, StatusCode, $q) {
        var item = this;

        item.getAll = function () {
            var deferred = $q.defer();
            return HttpService.GET(Api.host + Api.getItens).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

        item.cadastrar = function (params) {
            var deferred = $q.defer();
            return HttpService.POST(params, Api.host + Api.setItem).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        };
    }

    function FuncionarioService(HttpService, Api, StatusCode, $q) {
        var func = this;

        func.getFuncionarios = function (empresaId) {
            var deferred = $q.defer();
            return HttpService.GET(Api.host + Api.getFuncionarios.replace('{empresaId}', empresaId)).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }
    }

    function UsuarioService(HttpService, Api, StatusCode, $q) {
        var usu = this;

        usu.getUsuarios = function (empresaId) {
            var deferred = $q.defer();
            return HttpService.GET(Api.host + Api.getUsuarios.replace('{empresaId}', empresaId)).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

        usu.getTipoUsuarios = function () {
            var deferred = $q.defer();
            return HttpService.GET(Api.host + Api.getTipoUsuarios).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

        usu.getUsuario = function (usuarioId) {
            var deferred = $q.defer();
            return HttpService.GET(Api.host + Api.getUsuario.replace('{usuarioId}', usuarioId)).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

        usu.editar = function (params) {
            var deferred = $q.defer();
            return HttpService.PUT(params, Api.host + Api.putUsuario).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

        usu.cadastrar = function (params) {
            var deferred = $q.defer();
            return HttpService.POST(params, Api.host + Api.setUsuario).then(function (resp) {
                if (resp.status === StatusCode.Created) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

        usu.resetSenha = function (params) {
            var deferred = $q.defer();
            return HttpService.POST(params, Api.host + Api.resetSenha).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }

    }

    function ChamadoService(HttpService, Api, StatusCode, $q) {
        var ch = this;

        ch.setChamado = function (params) {
            var deferred = $q.defer();
            return HttpService.POST(params, Api.host + Api.setChamado).then(function (resp) {
                if (resp.status === StatusCode.OK) {
                    deferred.resolve(resp.data);
                } else {
                    deferred.reject();
                    console.error(resp.data);
                }
                return deferred.promise;
            });
        }
    }
})();