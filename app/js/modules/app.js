(function() {
    'use strict';

    angular
        .module('app', [
            'SignalR',
            'scos',
            'ui.router',
            'ngAnimate',
            'ngAria',
            'ngMessages',
            'ngMaterial',
            'ngStorage',
            'angular-linq',
            'ui.mask',
            'material.components.expansionPanels',
            'ngMap',
            'md.data.table',
            'ngPrint',
            'slick'
        ]).config(function($mdThemingProvider) {

            var customPrimary = {
                '50': '#1a83d1',
                '100': '#1775bb',
                '200': '#1566a4',
                '300': '#12588d',
                '400': '#0f4a77',
                '500': '#0c3c60',
                '600': '#092e49',
                '700': '#062033',
                '800': '#03111c',
                '900': '#010305',
                'A100': '#2290e3',
                'A200': '#389ce6',
                'A400': '#4fa7e9',
                'A700': '#000000'
            };
            $mdThemingProvider
                .definePalette('customPrimary',
                    customPrimary);

            var customAccent = {
                '50': '#2b5674',
                '100': '#316487',
                '200': '#38719a',
                '300': '#3f7fac',
                '400': '#498dbc',
                '500': '#5b98c3',
                '600': '#81b0d1',
                '700': '#93bbd8',
                '800': '#a6c7de',
                '900': '#b9d3e5',
                'A100': '#81b0d1',
                'A200': '#6ea4ca',
                'A400': '#5b98c3',
                'A700': '#cbdfec'
            };
            $mdThemingProvider
                .definePalette('customAccent',
                    customAccent);

            var customWarn = {
                '50': '#ff0310',
                '100': '#ff0310',
                '200': '#ff0310',
                '300': '#ff0310',
                '400': '#ff0310',
                '500': '#ff0310',
                '600': '#ff0310',
                '700': '#ff0310',
                '800': '#ff0310',
                '900': '#ff0310',
                'A100': '#ff0310',
                'A200': '#ff0310',
                'A400': '#ff0310',
                'A700': '#ff0310'
            };

            $mdThemingProvider
                .definePalette('customWarn',
                    customWarn);

            var customBackground = {
                '50': '#ffffff',
                '100': '#ffffff',
                '200': '#ffffff',
                '300': '#f5f8fa',
                '400': '#e3ecf3',
                '500': '#d1e0eb',
                '600': '#bfd4e3',
                '700': '#adc8dc',
                '800': '#9cbcd4',
                '900': '#8ab0cc',
                'A100': '#ffffff',
                'A200': '#ffffff',
                'A400': '#ffffff',
                'A700': '#78a4c4'
            };
            $mdThemingProvider
                .definePalette('customBackground',
                    customBackground);

            $mdThemingProvider.theme('default')
                .primaryPalette('customPrimary')
                .accentPalette('customAccent')
                .warnPalette('customWarn')
                .backgroundPalette('customBackground');

        }).filter("cep", function() {
            return function(a) {
                var b = a + "";
                return b = b.replace(/\D/g, ""),
                    b = b.replace(/^(\d{2})(\d{3})(\d)/, "$1.$2-$3")
            }
        })
        .filter("cnpj", function() {
            return function(a) {
                var b = a + "";
                return b = b.replace(/\D/g, ""), b = b.replace(/^(\d{2})(\d)/, "$1.$2"), b = b.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3"), b = b.replace(/\.(\d{3})(\d)/, ".$1/$2"), b = b.replace(/(\d{4})(\d)/, "$1-$2")
            }
        })
        .filter("cpf", function() {
            return function(a) {
                var b = a + "";
                return b = b.replace(/\D/g, ""), b = b.replace(/(\d{3})(\d)/, "$1.$2"), b = b.replace(/(\d{3})(\d)/, "$1.$2"), b = b.replace(/(\d{3})(\d{1,2})$/, "$1-$2")
            }
        })
        .filter("tel", function() {
            return function(a) {
                var b = a + "";
                return b = b.replace(/\D/g, ""), b = 11 === b.length ? b.replace(/^(\d{2})(\d{5})(\d{4})/, "($1) $2-$3") : b.replace(/^(\d{2})(\d{4})(\d{4})/, "($1) $2-$3")
            }
        })
        .config(function($stateProvider, $locationProvider) {

            // $locationProvider.html5Mode({
            //     enabled: true
            // });

            $stateProvider
                .state('login', {
                    url: '/',
                    views: {
                        'login-view': {
                            templateUrl: 'views/login.html',
                            controller: 'loginCtrl',
                            controllerAs: 'lg'
                        }
                    }
                }).state('inicial', {
                    url: '/Home',
                    views: {
                        'inicial-view': {
                            templateUrl: 'views/inicial.html',
                            controller: 'mainCtrl',
                            controllerAs: 'mn'
                        }
                    }
                }).state('inicial.mapa', {
                    url: '/Mapa',
                    views: {
                        'main-view': {
                            templateUrl: 'views/mapa.html',
                            controller: 'mapaCtrl',
                            controllerAs: 'mp'
                        }
                    }
                }).state('inicial.empresa', {
                    url: '/Clientes',
                    views: {
                        'main-view': {
                            templateUrl: 'views/empresa.html',
                            controller: 'empresaCtrl',
                            controllerAs: 'ep'
                        }
                    }
                }).state('inicial.chamado', {
                    url: '/Chamado',
                    views: {
                        'main-view': {
                            templateUrl: 'views/chamado.html',
                            controller: 'chamadoCtrl',
                            controllerAs: 'ch'
                        }
                    }
                }).state('inicial.chat', {
                    url: '/Chat',
                    views: {
                        'main-view': {
                            templateUrl: 'views/chat.html',
                            controller: 'chatCtrl',
                            controllerAs: 'chat'
                        }
                    }
                }).state('inicial.ordemservico', {
                    url: '/Ordem_de_Servico',
                    views: {
                        'main-view': {
                            templateUrl: 'views/ordemServico.html',
                            controller: 'ordemServicoCtrl',
                            controllerAs: 'os'
                        }
                    }
                }).state('inicial.impressao', {
                    url: '/Impressao/:id',
                    views: {
                        'main-view': {
                            templateUrl: 'views/impressao.html',
                            controller: 'impressaoCtrl',
                            controllerAs: 'im'
                        }
                    }
                }).state('inicial.usuario', {
                    url: '/Usuarios',
                    views: {
                        'main-view': {
                            templateUrl: 'views/usuario.html',
                            controller: 'usuarioCtrl',
                            controllerAs: 'us'
                        }
                    }
                });
        });
})();